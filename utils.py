import ast
import numpy as np
import os


def load_txt_file(file_path):
    result_dict = {}

    with open(file_path, 'r') as file:
        for line in file:
            key, value = line.strip().split(': ')
            result_dict[key] = ast.literal_eval(value)

    return result_dict


# helper functions
def save_dict_as_txt(output_path, dict):
    # save the stats to a txt
    with open(output_path, 'w') as file:
        for key, value in dict.items():
            file.write(f"{key}: {value}\n")


def save_dict_to_numpy_array(path, my_dict):
    keys = list(my_dict.keys())
    values = list(my_dict.values())
    values_array = np.array(values, dtype=object)
    # Stack keys and values as rows in a NumPy array
    result_array = np.vstack((keys, values_array))

    np.save(path, result_array)


def create_directories(base_path, datasets, modes):
    for dataset in datasets:
        dataset_folder = os.path.join(base_path, dataset)

        # Check if the dataset folder exists, and create it if not
        if not os.path.exists(dataset_folder):
            os.makedirs(dataset_folder)

        for mode in modes:
            mode_folder = os.path.join(dataset_folder, mode)

            # Check if the mode folder exists, and create it if not
            if not os.path.exists(mode_folder):
                os.makedirs(mode_folder)

            # Create a subfolder named "weights_biases" for every mode
            weights_biases_folder = os.path.join(mode_folder, "weights_biases")
            if not os.path.exists(weights_biases_folder):
                os.makedirs(weights_biases_folder)
