﻿# RepresentationLearning



This is the repo for the course “Deep Representation Learning”.

### Data Preparation
To run the code, first run the three data preprocessing scripts under /Data. They will download and preprocess the required data.

### Main script
The main script creates and trains the models and saves some statistics about them. The evaluation of the statistics is in the paper. 

*Note: The main script will also download a pretrained Word2Vec model (~3.6Gb), which is used to create an embedding for the textual data. If you wish to not do this, comment lines 379-386, then the Arxiv data will be ignored*


