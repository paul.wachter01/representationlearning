import os
import glob
from shutil import copyfile
import requests
import tarfile


def download_file(url, output_path):
    try:
        print("Downloading file")
        # Send a GET request to the URL
        response = requests.get(url, stream=True)
        response.raise_for_status()  # Raise an exception for bad responses

        # Open the file in binary mode and write the content
        with open(output_path, 'wb') as file:
            for chunk in response.iter_content(chunk_size=8192):
                file.write(chunk)

        print(f"Download successful. File saved to: {output_path}")
    except requests.exceptions.RequestException as e:
        print(f"Error downloading file: {e}")


def extract_tar_gz(file_path, extract_path):
    try:
        with tarfile.open(file_path, "r:gz") as tar:
            tar.extractall(path=extract_path)
        print(f"Extraction successful. Files extracted to: {extract_path}")
    except tarfile.TarError as e:
        print(f"Error extracting file: {e}")



symlink = False  # If this is false the files are copied instead
combine_train_valid = False  # If this is true, the train and valid sets are ALSO combined

# download the dataset
url = "https://datashare.ed.ac.uk/bitstream/handle/10283/3192/CINIC-10.tar.gz?sequence=4&isAllowed=y"
output_path = "./raw_data/CINIC-10.tar.gz"
os.makedirs("./raw_data", exist_ok=True)
download_file(url, output_path)

# extract the zip file
tar_file_path = "./raw_data/CINIC-10.tar.gz"
extract_to_path = "./raw_data/CINIC-10"
extract_tar_gz(tar_file_path, extract_to_path)

# extract all images that are not from cifar-10 and save them in Cinic data directory
cinic_directory = "./raw_data/CINIC-10"
output_directory = "./Cinic/data"
os.makedirs("./Cinic/data", exist_ok=True)
classes = ["airplane", "automobile", "bird", "cat", "deer", "dog", "frog", "horse", "ship", "truck"]
sets = ['train', 'valid', 'test']
if not os.path.exists(output_directory):
    os.makedirs(output_directory)
if not os.path.exists(output_directory + '/train'):
    os.makedirs(output_directory + '/train')
if not os.path.exists(output_directory + '/test'):
    os.makedirs(output_directory + '/test')

for c in classes:
    if not os.path.exists('{}/train/{}'.format(output_directory, c)):
        os.makedirs('{}/train/{}'.format(output_directory, c))
    if not os.path.exists('{}/test/{}'.format(output_directory, c)):
        os.makedirs('{}/test/{}'.format(output_directory, c))
    if not combine_train_valid:
        if not os.path.exists('{}/valid/{}'.format(output_directory, c)):
            os.makedirs('{}/valid/{}'.format(output_directory, c))

for s in sets:
    for c in classes:
        source_directory = '{}/{}/{}'.format(cinic_directory, s, c)
        filenames = glob.glob('{}/*.png'.format(source_directory))
        for fn in filenames:
            dest_fn = fn.split('/')[-1]
            if (s == 'train' or s == 'valid') and combine_train_valid and 'cifar' not in fn.split('/')[-1]:
                dest_fn = '{}/train/{}/{}'.format(output_directory, c, dest_fn)
                if symlink:
                    if not os.path.islink(dest_fn):
                        os.symlink(fn, dest_fn)
                else:
                    copyfile(fn, dest_fn)

            elif (s == 'train') and 'cifar' not in fn.split('/')[-1]:
                dest_fn = '{}/train/{}/{}'.format(output_directory, c, dest_fn)
                if symlink:
                    if not os.path.islink(dest_fn):
                        os.symlink(fn, dest_fn)
                else:
                    copyfile(fn, dest_fn)

            elif (s == 'valid') and 'cifar' not in fn.split('/')[-1]:
                dest_fn = '{}/valid/{}/{}'.format(output_directory, c, dest_fn)
                if symlink:
                    if not os.path.islink(dest_fn):
                        os.symlink(fn, dest_fn)
                else:
                    copyfile(fn, dest_fn)

            elif s == 'test' and 'cifar' not in fn.split('/')[-1]:
                dest_fn = '{}/test/{}/{}'.format(output_directory, c, dest_fn)
                if symlink:
                    if not os.path.islink(dest_fn):
                        os.symlink(fn, dest_fn)
                else:
                    copyfile(fn, dest_fn)
