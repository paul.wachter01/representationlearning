import pandas as pd
import os
import requests
import zipfile


def download_file(url, output_path):
    try:
        # Send a GET request to the URL
        response = requests.get(url, stream=True)
        response.raise_for_status()  # Raise an exception for bad responses

        # Open the file in binary mode and write the content
        with open(output_path, 'wb') as file:
            for chunk in response.iter_content(chunk_size=8192):
                file.write(chunk)

        print(f"Download successful. File saved to: {output_path}")
    except requests.exceptions.RequestException as e:
        print(f"Error downloading file: {e}")


def extract_zip(zip_file_path, extract_to_path):
    try:
        with zipfile.ZipFile(zip_file_path, 'r') as zip_ref:
            zip_ref.extractall(extract_to_path)
        print(f"Extraction successful. Files extracted to: {extract_to_path}")
    except zipfile.BadZipFile as e:
        print(f"Error: {e}")
    except Exception as e:
        print(f"Error extracting zip file: {e}")


# First, download the file
url = "https://github.com/ashfarhangi/Protoformer/raw/main/data/ArXiv-10.zip"
os.makedirs("./raw_data", exist_ok=True)
output_path = "./raw_data/ArXiv-10.zip"
download_file(url, output_path)

# Extract zip file
zip_file_path = "./raw_data/ArXiv-10.zip"
extract_to_path = "./raw_data"
extract_zip(zip_file_path, extract_to_path)

# Convert the csv to txt files
# Path to your CSV file
csv_file_path = './raw_data/arxiv100.csv'
csv_output_path = "./Arxiv/data/"
os.makedirs(csv_output_path, exist_ok=True)

# Read the CSV file
df = pd.read_csv(csv_file_path)

# Iterate through each row in the DataFrame
for index, row in df.iterrows():
    # Extract the first 32 words from the second column
    words = row.iloc[1].split()[:32]

    # Get the folder name from the third column
    folder_name = str(row.iloc[2])

    # Create the folder if it doesn't exist
    folder_path = os.path.join(csv_output_path, folder_name)
    os.makedirs(folder_path, exist_ok=True)

    # Save the words as a text file in the folder
    output_file_path = os.path.join(folder_path, f"{index}_output.txt")
    with open(output_file_path, 'w', encoding='utf-8') as output_file:
        output_file.write(' '.join(words))

print("Extraction and saving complete.")
