import os
import shutil
import random
import zipfile


def delete_folders(folder_names, base_path='.'):
    for folder_name in folder_names:
        folder_path = os.path.join(base_path, folder_name)
        try:
            # Check if the folder exists before attempting to delete
            if os.path.exists(folder_path):
                # Delete the folder and its contents
                shutil.rmtree(folder_path)  # Use os.rmdir to delete an empty directory
                print(f"Folder '{folder_name}' deleted successfully.")
            else:
                print(f"Folder '{folder_name}' not found.")
        except Exception as e:
            print(f"Error deleting folder '{folder_name}': {e}")


def split_images(input_folder, output_folder):
    # Create output folders for each split
    os.makedirs(os.path.join(output_folder, 'valid'))
    os.makedirs(os.path.join(output_folder, 'test'))
    os.makedirs(os.path.join(output_folder, 'train'))

    # List all folders in the input directory
    folders = os.listdir(input_folder)

    for folder in folders:
        folder_path = os.path.join(input_folder, folder)
        output_folder_20 = os.path.join(output_folder, 'valid', folder)
        output_folder_40 = os.path.join(output_folder, 'test', folder)
        output_folder_60 = os.path.join(output_folder, 'train', folder)

        # Create output folders for each split within the current folder
        os.makedirs(output_folder_20)
        os.makedirs(output_folder_40)
        os.makedirs(output_folder_60)

        # List all images in the current folder
        images = os.listdir(folder_path)

        # Calculate the number of images for each split
        num_images_20 = int(0.2 * len(images))
        num_images_40 = int(0.2 * len(images))

        # Randomly select images for each split
        random.shuffle(images)
        images_20 = images[:num_images_20]
        images_40 = images[num_images_20:num_images_20 + num_images_40]
        images_60 = images[num_images_20 + num_images_40:]

        # Copy selected images to the corresponding output folders
        for image in images_20:
            shutil.copy(os.path.join(folder_path, image), os.path.join(output_folder_20, image))

        for image in images_40:
            shutil.copy(os.path.join(folder_path, image), os.path.join(output_folder_40, image))

        for image in images_60:
            shutil.copy(os.path.join(folder_path, image), os.path.join(output_folder_60, image))


def extract_zip(zip_file_path, extract_to_path):
    try:
        with zipfile.ZipFile(zip_file_path, 'r') as zip_ref:
            zip_ref.extractall(extract_to_path)
        print(f"Extraction successful. Files extracted to: {extract_to_path}")
    except zipfile.BadZipFile as e:
        print(f"Error: {e}")
    except Exception as e:
        print(f"Error extracting zip file: {e}")


zip_file_path = "./raw_data/archive_flowers.zip"
extract_to_path = "./raw_data"
extract_zip(zip_file_path, extract_to_path)

# only keep the 10 categories with the most images
folders_to_delete = ['astilbe', 'bellflower', 'calendula', 'carnation', 'common_daisy', 'daffodil']
base_directory = './raw_data/flowers'
delete_folders(folders_to_delete, base_directory)

# split the data in train, test, valid
# split the data in train, test, valid
input_path = './raw_data/flowers'
output_path = './Flowers/data'
split_images(input_path, output_path)
