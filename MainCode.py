import tensorflow as tf
from keras import layers, models, initializers
from keras.preprocessing.image import ImageDataGenerator
from keras.callbacks import EarlyStopping
import numpy as np
from scipy import stats
import os
import gensim.downloader
from utils import save_dict_as_txt, save_dict_to_numpy_array, create_directories


"""
Arxiv text files processing
"""

def load_and_process_arxiv_text_files(file_path, word2vec_model):
    with open(file_path, 'r', encoding='utf-8') as file:
        embeddings = []
        for line in file:
            words = line.lower().split()
            if len(words) >= 32:
                embeddings.extend(
                    [word2vec_model[word][:96] if word in word2vec_model else np.zeros(96) for word in words])

        return np.array(embeddings)[:32 * 96].reshape((32, 96))


def create_dataset_from_text_file_folder(folder_path, word2vec_model, processed_folder="processed"):
    examples = []
    labels = []

    processed_folder_path = os.path.join("./Data/Arxiv/data/", processed_folder)
    os.makedirs(processed_folder_path, exist_ok=True)

    categories_dict = {"astro-ph": 0, "cond-mat": 1, "cs": 2, "eess": 3, "hep-ph": 4, "hep-th": 5, "math": 6,
                       "physics": 7, "quant-ph": 8, "stat": 9}

    categories = os.listdir(folder_path)
    for category in categories:
        category_path = os.path.join(folder_path, category)

        if os.path.isdir(category_path):
            category_processed_folder = os.path.join(processed_folder_path, category)
            os.makedirs(category_processed_folder, exist_ok=True)

            for file_name in os.listdir(category_path):
                file_path = os.path.join(category_path, file_name)

                if os.path.isfile(file_path) and file_name.endswith('.txt'):
                    try:
                        embeddings = load_and_process_arxiv_text_files(file_path, word2vec_model)
                        if np.array(embeddings).shape == (32, 96):
                            labels.append(categories_dict[category])
                            examples.append(embeddings)
                    except:
                        continue

    return np.array(examples), np.array(labels)


# data generator
def arxive_dataset_creator(data_folder_path, batch_size, word2vec_model):
    # Create a dataset from the provided folder
    examples, labels = create_dataset_from_text_file_folder(data_folder_path, word2vec_model)

    # Reshape each example to have the shape 32*32*3
    examples = examples.reshape((-1, 32, 32, 3))
    unique_elements, counts = np.unique(labels, return_counts=True)

    train_labels_one_hot = tf.one_hot(labels, depth=10)
    # Create a TensorFlow Dataset
    dataset = tf.data.Dataset.from_tensor_slices((examples, train_labels_one_hot))

    # Shuffle and batch the dataset
    dataset = dataset.shuffle(buffer_size=len(labels))

    # Calculate the sizes of train, validation, and test sets
    total_size = len(labels)
    train_size = int(0.6 * total_size)  # 70% for training
    val_size = int(0.2 * total_size)  # 15% for validation
    test_size = int(0.2 * total_size)  # Remaining for test

    # Split the dataset
    train_dataset = dataset.take(train_size).batch(batch_size)
    remaining_dataset = dataset.skip(train_size)
    val_dataset = remaining_dataset.take(val_size).batch(batch_size)
    test_dataset = remaining_dataset.skip(val_size).batch(batch_size)

    return train_dataset, val_dataset, test_dataset


"""
Data generators for other datasets
"""


def get_train_data_generator(path, val_split=None):
    if val_split:
        data_generator = ImageDataGenerator(
            rescale=1. / 255,
            validation_split=0.2
        )

        train_generator = data_generator.flow_from_directory(
            path,
            target_size=(input_shape[0], input_shape[1]),
            batch_size=batch_size,
            seed=42,
            shuffle=True,
            class_mode='categorical',
            subset="training"
        )

    else:

        train_generator = ImageDataGenerator(rescale=1. / 255).flow_from_directory(
            path,
            target_size=(input_shape[0], input_shape[1]),
            batch_size=batch_size,
            seed=42,
            shuffle=True,
            class_mode='categorical',

        )
    return train_generator


def get_val_data_generator(path, val_split=None):
    if val_split:
        data_generator = ImageDataGenerator(
            rescale=1. / 255,
            validation_split=0.2
        )
        validation_generator = data_generator.flow_from_directory(
            path,
            target_size=(input_shape[0], input_shape[1]),
            batch_size=batch_size,
            seed=42,
            subset="validation"
        )
    else:

        validation_generator = ImageDataGenerator(rescale=1. / 255).flow_from_directory(
            path,
            target_size=(input_shape[0], input_shape[1]),
            batch_size=batch_size,
            seed=42
        )

    return validation_generator


def get_test_data_generator(path):
    test_generator = ImageDataGenerator(rescale=1. / 255).flow_from_directory(
        path,
        target_size=(input_shape[0], input_shape[1]),
        batch_size=batch_size,
        shuffle=False
    )
    return test_generator


"""
Models and training
"""


def create_custom_LeNet_model(initializer, costum_ini_params=None):
    model = models.Sequential()
    print(initializer)
    for i in range(5):
        if costum_ini_params:
            layer_name = f'layer{i}'
            params = costum_ini_params[layer_name]

            np.random.seed(i)
            weight_initializer = initializers.RandomNormal(mean=params[0], stddev=params[1])
            np.random.seed(i + 10)
            bias_initializer = initializers.RandomNormal(mean=params[2], stddev=params[3])
        elif initializer == "Random":
            np.random.seed(i)
            weight_initializer = initializers.RandomNormal()
            np.random.seed(i+10)
            bias_initializer = initializers.RandomNormal()
        elif initializer == "Glorot":
            np.random.seed(i)
            weight_initializer = initializers.GlorotNormal()
            np.random.seed(i+10)
            bias_initializer = initializers.GlorotNormal()

        # Determine layer type based on the layer number (even for Conv2D, odd for Dense)
        if i == 0:
            layer = layers.Conv2D(6, (5, 5), activation='relu',
                                  input_shape=(32, 32, 3),
                                  kernel_initializer=weight_initializer,
                                  bias_initializer=bias_initializer)
            model.add(layer)

            model.add(layers.MaxPooling2D((2, 2)))


        elif i == 1:
            layer = layers.Conv2D(16, (5, 5), activation='relu',
                                  input_shape=(32, 32, 3),
                                  kernel_initializer=weight_initializer,
                                  bias_initializer=bias_initializer)
            model.add(layer)

            model.add(layers.MaxPooling2D((2, 2)))
            model.add(layers.Flatten())

        elif i == 2:
            layer = layers.Dense(120, activation='relu',
                                 kernel_initializer=weight_initializer,
                                 bias_initializer=bias_initializer)
            model.add(layer)

        elif i == 3:
            layer = layers.Dense(84, activation='relu',
                                 kernel_initializer=weight_initializer,
                                 bias_initializer=bias_initializer)
            model.add(layer)

        elif i == 4:
            layer = layers.Dense(10, activation='softmax',
                                 kernel_initializer=weight_initializer,
                                 bias_initializer=bias_initializer)
            model.add(layer)

    return model


def train_model(model, train_data, val_data, test_data, n_epochs, n):
    early_stopping = EarlyStopping(monitor='val_loss', patience=10, restore_best_weights=True)
    stats = {}

    model.compile(optimizer='sgd', loss='categorical_crossentropy', metrics=['accuracy'])
    model.summary()
    history = model.fit(train_data, epochs=n_epochs, validation_data=val_data, callbacks=[early_stopping])
    test_results = model.evaluate(test_data)

    print(f'Test Accuracy: {test_results[1] * 100:.2f}%')
    stats["model_" + str(n) + "_train_acc"] = history.history['accuracy']
    stats["model_" + str(n) + "_val_acc"] = history.history['val_accuracy']
    stats["model_" + str(n) + "_test_acc"] = f'Test Accuracy: {test_results[1] * 100:.2f}%'

    return model, stats


def train_and_save_models(dataset, mode, num_models, train_data_gen, val_data_gen, test_data_gen,
                          mean_std_of_each_layer=None):
    stats_all_models = {}
    for i in range(num_models):
        if mode == "Random" and dataset != "Arxiv":
            model = create_custom_LeNet_model(mode)
        else:
            model = create_custom_LeNet_model(mode, mean_std_of_each_layer)

        trained_model, model_stats = train_model(model, train_data_gen, val_data_gen, test_data_gen, n_epochs, i)

        stats_all_models.update(model_stats)
        save_dict_to_numpy_array(f"./{dataset}/{mode}/weights_biases/model{i}.np", extract_weights(model))

    save_dict_as_txt(f"./{dataset}/{mode}/stats.txt", stats_all_models)


"""
Underlying distribution extraction
"""


def extract_weights(model):
    model_weights = {}
    # Extract weights and biases for each layer
    for j, layer in enumerate(model.layers):
        if isinstance(layer, (layers.Conv2D, layers.Dense)):
            weights, biases = layer.get_weights()
            layer_name = f"layer{j + 1}_weights"
            model_weights[layer_name] = weights
            layer_name = f"layer{j + 1}_biases"
            model_weights[layer_name] = biases

    return model_weights


def combine_weights_biases_of_models(weights_dict, n_layers):
    # Create lists for weights and biases of each layer depth
    all_weights_biases_per_layer = {}

    # combine the weights and biases for each layer from all models
    for i in range(n_layers):
        weights_list = []
        biases_list = []

        for model_name, model_weights in weights_dict.items():
            layer_name = f"layer{i + 1}"
            weights_list.extend(model_weights.get(f"{layer_name}_weights", []))
            biases_list.extend(model_weights.get(f"{layer_name}_biases", []))

        all_weights_biases_per_layer[f"list{i + 1}_weights"] = weights_list
        all_weights_biases_per_layer[f"list{i + 1}_biases"] = biases_list

    return all_weights_biases_per_layer


def get_mean_std_for_each_layer(all_weights_biases_per_layer, n_layers):
    # Calculate mean and std of weights and biases for each layer
    layer_params_dict = {}
    for i in range(1, n_layers + 1):
        weights_list = all_weights_biases_per_layer[f"list{i}_weights"]
        biases_list = all_weights_biases_per_layer[f"list{i}_biases"]

        mean_weights, std_weights = stats.norm.fit(weights_list)
        mean_biases, std_biases = stats.norm.fit(biases_list)
        if i == 1 or i == 3 or i == 8 or i == 6 or i == 7:
            lengh = len(layer_params_dict)
            layer_params_dict[f"layer{lengh}"] = [mean_weights, std_weights, mean_biases, std_biases]
    return layer_params_dict


# parameters for model creation and training
num_models = 10
batch_size = 64
n_epochs = 200
input_shape = (32, 32)
datasets = ["Cifar10", "Cinic", "Flowers", "Arxiv"]
modes = ["Random", "Transferred", "Glorot"]
base_path = "./"

create_directories(base_path, datasets, modes)

# Cifar10 Data
train_data_gen = get_train_data_generator("./Data/Cifar10/data/train", 0.2)
val_data_gen = get_val_data_generator("./Data/Cifar10/data/train", 0.2)
test_data_gen = get_test_data_generator("./Data/Cifar10/data/test")

# Initial model on Cifar10
trained_base_models = []
stats_all_models = {}
for i in range(num_models):
    model = create_custom_LeNet_model("Random")
    trained_model, model_stats = train_model(model, train_data_gen, val_data_gen, test_data_gen, n_epochs, i)

    stats_all_models.update(model_stats)
    save_dict_to_numpy_array(f"./Cifar10/Random/weights_biases/model{i}.np", extract_weights(model))
    trained_base_models.append(trained_model)

save_dict_as_txt("./Cifar10/Random/stats.txt", stats_all_models)

# Compute mean and std of the base model for each layer
weights_all_models = {f"model{number}": extract_weights(model) for number, model in enumerate(trained_base_models)}
combined_weights_biases = combine_weights_biases_of_models(weights_all_models, 8)
mean_std_of_each_layer = get_mean_std_for_each_layer(combined_weights_biases, 8)
save_dict_as_txt("./Cifar10/Random/mean_std_stats.txt", mean_std_of_each_layer)

# Train models on Cifar10 in each mode
for mode in modes:
    if mode == "Random":
        continue
    train_and_save_models("Cifar10", mode, num_models, train_data_gen, val_data_gen, test_data_gen,
                          mean_std_of_each_layer)

# Train models on Cinc10
train_data_gen = get_train_data_generator("./Data/Cinic/data/train")
val_data_gen = get_val_data_generator("./Data/Cinic/data/valid")
test_data_gen = get_test_data_generator("./Data/Cinic/data/test")

for mode in modes:
    train_and_save_models("Cinic", mode, num_models, train_data_gen, val_data_gen, test_data_gen,
                          mean_std_of_each_layer)

# Train models on Flowers
train_data_gen = get_train_data_generator("./Data/Flowers/data/train")
val_data_gen = get_val_data_generator("./Data/Flowers/data/valid")
test_data_gen = get_test_data_generator("./Data/Flowers/data/test")

for mode in modes:
    train_and_save_models("Flowers", mode, num_models, train_data_gen, val_data_gen, test_data_gen,
                          mean_std_of_each_layer)

# download the pretrained word2vec model
word2vec_model = gensim.downloader.load("word2vec-google-news-300")

# Train models on Arxiv
train_data, val_data, test_data = arxive_dataset_creator(base_path + "Data/Arxiv/data", batch_size, word2vec_model)

for mode in modes:
    train_and_save_models("Arxiv", mode, num_models, train_data, val_data, test_data, mean_std_of_each_layer)
